json.extract! person, :id, :name, :age, :date, :obs, :created_at, :updated_at
json.url person_url(person, format: :json)
