class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :name
      t.integer :age
      t.date :date
      t.string :obs

      t.timestamps
    end
  end
end
